package stefana.com.basket.dtos;

import java.util.HashMap;
import java.util.Map;

public class AppointmentDTO {

    private long startTime;
    private String description;
    private Map<String, String> players = new HashMap<>();
    private String id;

    public AppointmentDTO() {
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getPlayers() {
        return players;
    }

    public void setPlayers(Map<String, String> players) {
        this.players = players;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
