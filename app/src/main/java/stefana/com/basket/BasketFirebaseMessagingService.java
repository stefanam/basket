package stefana.com.basket;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import stefana.com.basket.activities.ListOfPlayersActivity;

import static stefana.com.basket.activities.BasketAppointmentsActivity.APPOINTMENT_ID;
import static stefana.com.basket.activities.SettingsActivity.NOTIFICATION_SETTING;

public class BasketFirebaseMessagingService extends FirebaseMessagingService {

    private int notifcationCounter;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (!preferences.contains(NOTIFICATION_SETTING) || preferences.getBoolean(NOTIFICATION_SETTING, false)) {
            String appid = remoteMessage.getData().get("appointmentId");

            Intent intent = new Intent(this, ListOfPlayersActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(APPOINTMENT_ID, appid);
            //problem sa ne prosledjivanjem extra kroz intent, reseno je dodavanjem flega na kraju za pending intent
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification.Builder mBuilder = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(remoteMessage.getData().get("title"))
                    .setContentText(remoteMessage.getData().get("message"))
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);


            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String channelId = "stefana";
                NotificationChannel channel = new NotificationChannel(channelId,
                        "Notifikacije o prijavama",
                        NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
                mBuilder.setChannelId(channelId);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                notificationManager.notify(notifcationCounter++, mBuilder.build());
            } else {
                notificationManager.notify(notifcationCounter++, mBuilder.getNotification());
            }
        }
    }
}
