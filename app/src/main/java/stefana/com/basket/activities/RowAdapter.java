package stefana.com.basket.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import stefana.com.basket.R;
import stefana.com.basket.dtos.AppointmentDTO;

public class RowAdapter extends RecyclerView.Adapter<RowViewHolder> {

    List<AppointmentDTO> appointments = new ArrayList<>();
    private RecyclerViewClickListener mListener;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy  HH:mm");

    public RowAdapter(List<AppointmentDTO> appointments) {
        this.appointments = appointments;
    }

    @NonNull
    @Override
    public RowViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();

        View v = LayoutInflater
                .from(context)
                .inflate(R.layout.row_view_holder, viewGroup, false);

        return new RowViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RowViewHolder rowViewHolder, int position) {

        AppointmentDTO appointmentDTO = appointments.get(position);
        rowViewHolder.appointment = appointmentDTO;
        String formatedDate = simpleDateFormat.format(new Date(appointmentDTO.getStartTime()));
        rowViewHolder.timeAndDate.setText("Datum i vreme: " + formatedDate);
        rowViewHolder.desc.setText("Opis: " + appointmentDTO.getDescription());
        rowViewHolder.numberOfPlayersForAppointment.setText("Broj prijavljenih: " + appointmentDTO.getPlayers().size());
    }

    @Override
    public int getItemCount() {
        return appointments.size();
    }

    public void setItems(ArrayList<AppointmentDTO> appointments) {
        this.appointments = appointments;
    }
}
