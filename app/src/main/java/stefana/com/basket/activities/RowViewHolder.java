package stefana.com.basket.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import stefana.com.basket.R;
import stefana.com.basket.dtos.AppointmentDTO;

import static stefana.com.basket.activities.BasketAppointmentsActivity.APPOINTMENT_ID;

public class RowViewHolder extends RecyclerView.ViewHolder {

    public RecyclerViewClickListener listener;
    public TextView timeAndDate;
    public TextView desc;
    public TextView numberOfPlayersForAppointment;
    public AppointmentDTO appointment;

    public RowViewHolder(@NonNull final View itemView, RecyclerViewClickListener listener) {
        super(itemView);
        this.listener = listener;
        timeAndDate = itemView.findViewById(R.id.date_time_view_holder_tv);
        desc = itemView.findViewById(R.id.opis_view_holder_tv);
        numberOfPlayersForAppointment = itemView.findViewById(R.id.br_prijavljenih);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(itemView.getContext(), ListOfPlayersActivity.class);
                intent.putExtra(APPOINTMENT_ID, appointment.getId());
                itemView.getContext().startActivity(intent);
            }
        });
    }

    public RowViewHolder(View v) {
        super(v);
    }
}
