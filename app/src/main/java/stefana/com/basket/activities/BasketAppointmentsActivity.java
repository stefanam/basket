package stefana.com.basket.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Comparator;

import stefana.com.basket.R;
import stefana.com.basket.dtos.AppointmentDTO;

public class BasketAppointmentsActivity extends AppCompatActivity {

    public static String APPOINTMENT_ID = "APPOINTMENT_ID";
    private ArrayList<AppointmentDTO> appointments = new ArrayList<>();
    private RecyclerView recyclerView;
    private RowAdapter adapter;
    private SwipeRefreshLayout swiper;
    private AppointmentDTO appointment;
    public SwipeController swipeController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket_appointments);
        swiper = findViewById(R.id.swiperefresh);
        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BasketAppointmentsActivity.this, NewAppointmentActivity.class);
                startActivity(intent);
            }
        });
        loadAppointments();
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(llm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new SpacesItemDecoration(12));
        adapter = new RowAdapter(appointments);
        recyclerView.setAdapter(adapter);
        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadAppointments();
                swiper.setRefreshing(false);
            }
        });

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(final int position) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(BasketAppointmentsActivity.this);
                builder.setTitle("Potvrdi");
                builder.setMessage("Da li ste sigurni da želite da obrišete ovaj termin?");
                builder.setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        AppointmentDTO appointment = adapter.appointments.get(position);
                        adapter.appointments.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(position, adapter.getItemCount());
                        FirebaseDatabase.getInstance().getReference(appointment.getId()).removeValue();

                    }
                });
                builder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                builder.show();
            }

        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeController);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void loadAppointments() {
        FirebaseDatabase
                .getInstance()
                .getReference()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        appointments = new ArrayList<>();

                        //load
                        for (DataSnapshot noteSnapshot : dataSnapshot.getChildren()) {
                            appointment = noteSnapshot.getValue(AppointmentDTO.class);
                            appointment.setId(noteSnapshot.getKey());
                            //Log.e("Stefana", appointment.getStartTime() + " " + appointment.getDescription());
                            appointments.add(appointment);

                            //sort
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                appointments.sort(new Comparator<AppointmentDTO>() {
                                    @Override
                                    public int compare(AppointmentDTO o1, AppointmentDTO o2) {
                                        if (o1.getStartTime() < o2.getStartTime()) {
                                            return 1;
                                        } else if (o1.getStartTime() > o2.getStartTime()) {
                                            return -1;
                                        } else {
                                            return 0;
                                        }
                                    }
                                });

                            }

                            adapter.setItems(appointments);
                            adapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_basket_appointment, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.logout) {
            logout();
        } else if (item.getItemId() == R.id.settings){
            Intent intent = new Intent(BasketAppointmentsActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void logout() {
        FirebaseAuth.getInstance().signOut();
        Intent i = new Intent(BasketAppointmentsActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        logout();
        Intent intent = new Intent(BasketAppointmentsActivity.this, LoginActivity.class);
        startActivity(intent);
    }



}
