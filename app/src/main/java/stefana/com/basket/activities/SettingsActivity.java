package stefana.com.basket.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.widget.CompoundButton;

import stefana.com.basket.R;

public class SettingsActivity extends AppCompatActivity {

    private SwitchCompat switchCompat;
    public static String NOTIFICATION_SETTING = "NOTIFICATION_SETTING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        switchCompat = findViewById(R.id.ukljuci_notifikacije);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if(preferences.contains(NOTIFICATION_SETTING)){
            switchCompat.setChecked(getPref(NOTIFICATION_SETTING, this));
        }else{
            switchCompat.setChecked(true);
            putPref(NOTIFICATION_SETTING, true, this);
        }

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                putPref(NOTIFICATION_SETTING, isChecked, SettingsActivity.this);
            }
        });
    }

    public static void putPref(String key, boolean value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getPref(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, false);
    }
}
