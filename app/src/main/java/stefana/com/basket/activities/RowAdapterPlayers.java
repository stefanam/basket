package stefana.com.basket.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import stefana.com.basket.R;

public class RowAdapterPlayers extends RecyclerView.Adapter<RowViewHolderPlayers> {

    List<String> players = new ArrayList<>();

    public RowAdapterPlayers(List<String> players) {
        this.players = players;
    }

    @NonNull
    @Override
    public RowViewHolderPlayers onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();

        View v = LayoutInflater
                .from(context)
                .inflate(R.layout.player_view_holder, viewGroup, false);

        return new RowViewHolderPlayers(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RowViewHolderPlayers rowViewHolderPlayers, int position) {
        String playerDTO = players.get(position);
        rowViewHolderPlayers.nickOrName.setText(String.valueOf(playerDTO));
    }

    @Override
    public int getItemCount() {
        return players.size();
    }
}
