package stefana.com.basket.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import stefana.com.basket.R;
import stefana.com.basket.dtos.AppointmentDTO;

public class NewAppointmentActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private DatePicker datePicker;
    private TimePicker timePicker;
    private Button cancel;
    private Button save;
    private AppointmentDTO appointment;
    private EditText description;
    private ArrayList<AppointmentDTO> appointments;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd.MM.yyyy");


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_appointment);
        tabLayout = findViewById(R.id.tabLayout2);
        datePicker = findViewById(R.id.date_picker);
        timePicker = findViewById(R.id.time_picker);
        cancel = findViewById(R.id.odustani);
        save = findViewById(R.id.zakazi);
        description = findViewById(R.id.opis);
        appointment = new AppointmentDTO();


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    if (datePicker.getVisibility() == View.GONE) {
                        timePicker.setVisibility(View.GONE);
                        datePicker.setVisibility(View.VISIBLE);
                    }
                }

                if (tab.getPosition() == 1) {
                    if (timePicker.getVisibility() == View.GONE) {
                        datePicker.setVisibility(View.GONE);
                        timePicker.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int h = 0, m = 0;

                if (Build.VERSION.SDK_INT < 23) {
                    h = timePicker.getCurrentHour();
                    m = timePicker.getCurrentMinute();
                } else {
                    h = timePicker.getHour();
                    m = timePicker.getMinute();
                }

                appointment.setDescription(description.getText().toString());

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, h);//?????
                calendar.set(Calendar.MINUTE, m);
                calendar.set(Calendar.YEAR, datePicker.getYear());
                calendar.set(Calendar.MONTH, datePicker.getMonth());
                calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());

                appointment.setStartTime(calendar.getTimeInMillis());
                //Log.e("Datum: ", String.valueOf(appointment.getStartTime()));
                makeAppointment(appointment);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewAppointmentActivity.this, BasketAppointmentsActivity.class);
                startActivity(intent);
            }
        });


    }


    private void makeAppointment(final AppointmentDTO appointment) {
        String id = UUID.randomUUID().toString();
        appointment.setId(id);
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child(id)
                .setValue(appointment)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        ListOfPlayersActivity.sendMessage(NewAppointmentActivity.this, "new_appointment",
                                "Zakazan je novi termin za " + simpleDateFormat.format(new Date(appointment.getStartTime())), null, appointment);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        Toast.makeText(NewAppointmentActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


}
