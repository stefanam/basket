package stefana.com.basket.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import stefana.com.basket.R;

import static stefana.com.basket.activities.LoginActivity.EMAIL_ADDRESS_PATTERN;

public class RegistrationActivity extends AppCompatActivity {

    private EditText email;
    private EditText nickName;
    private EditText password;
    private Button register;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        email = findViewById(R.id.email_register);
        password = findViewById(R.id.password_register);
        nickName = findViewById(R.id.nickname_register);
        register = findViewById(R.id.email_sign_in_button);

        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .requestProfile()
                .build();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkIfPassIsConfirmed(password) == true && checkEmail(email) == true) {
                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                       @Override
                                                       public void onComplete(@NonNull Task<AuthResult> task) {
                                                           if (task.isSuccessful()) {
                                                               FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                                               UserProfileChangeRequest request = new UserProfileChangeRequest.Builder()
                                                                       .setDisplayName(nickName.getText().toString()).build();
                                                               user.updateProfile(request);
                                                               Toast.makeText(RegistrationActivity.this, "You are successfully signed in!",
                                                                       Toast.LENGTH_SHORT).show();
                                                               // Sign in success, update UI with the signed-in user's information
                                                               Intent i = new Intent(RegistrationActivity.this, BasketAppointmentsActivity.class);
                                                               startActivity(i);


                                                           } else {
                                                               // If sign in fails, display a message to the user.
                                                               Toast.makeText(RegistrationActivity.this, task.getException().getLocalizedMessage(),
                                                                       Toast.LENGTH_SHORT).show();
                                                           }
                                                       }
                                                   }
                            );
                } else {
                    Toast.makeText(RegistrationActivity.this, "Invalid mail or password", Toast.LENGTH_SHORT).show();
                    cancelIfNotValid(email, password);
                }
            }
        });

    }


    private boolean checkIfPassIsConfirmed(EditText password) {

        if (password.getText().toString().length() < 5) {
            Toast.makeText(RegistrationActivity.this, "Password doesn't match", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }



    private void cancelIfNotValid(EditText email, EditText firstPass) {

        email.setText("");
        firstPass.setText("");
    }

    private boolean checkEmail(EditText email) {

        return EMAIL_ADDRESS_PATTERN.matcher(email.getText().toString()).matches();
    }
}
