package stefana.com.basket.activities;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import stefana.com.basket.R;

public class RowViewHolderPlayers extends RecyclerView.ViewHolder {

    public TextView nickOrName;


    public RowViewHolderPlayers(@NonNull View itemView) {
        super(itemView);
        nickOrName = itemView.findViewById(R.id.name_of_player_that_play);
    }
}
