package stefana.com.basket.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import stefana.com.basket.R;
import stefana.com.basket.dtos.AppointmentDTO;
import stefana.com.basket.dtos.DataDTO;
import stefana.com.basket.dtos.SendMessageRequestDTO;

import static stefana.com.basket.activities.BasketAppointmentsActivity.APPOINTMENT_ID;

public class ListOfPlayersActivity extends AppCompatActivity {

    private static final String YOUR_LEGACY_SERVER_KEY_FROM_FIREBASE_CONSOLE = "AIzaSyCqUWtKe4QbOn8eE__j6I6YIW4fE89tArg";


    private Button play;
    private RecyclerView recyclerView;
    private RowAdapterPlayers adapter;
    private AppointmentDTO appointmentDTO;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd.MM.yyyy");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_players);
        int whiteColor = ContextCompat.getColor(this, android.R.color.white);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
//        toolbar.setTitleTextColor(whiteColor);
//        toolbar.setSubtitleTextColor(whiteColor);

        play = findViewById(R.id.play);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView = findViewById(R.id.recycler_view_list_of_players);
        recyclerView.setLayoutManager(llm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new SpacesItemDecoration(12));
        adapter = new RowAdapterPlayers(new ArrayList<String>());
        recyclerView.setAdapter(adapter);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
                if (appointmentDTO.getPlayers().containsKey(name)) {
                    //save appointment
                    //ako je vec prijavljen obrisi ga i sacuvaj novo stanje
                    appointmentDTO.getPlayers().remove(name);
                    FirebaseDatabase
                            .getInstance()
                            .getReference()
                            .child(appointmentDTO.getId())
                            .child("players")
                            .setValue(appointmentDTO.getPlayers())
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    unsubscribe(appointmentDTO.getId(), new Runnable() {
                                        @Override
                                        public void run() {
                                            sendMessage(ListOfPlayersActivity.this, appointmentDTO.getId(), FirebaseAuth.getInstance().getCurrentUser().getDisplayName() +
                                                    " se odjavio sa termina " + simpleDateFormat.format(new Date(appointmentDTO.getStartTime())), null, appointmentDTO);
                                        }
                                    });

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(ListOfPlayersActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                } else {//save appointment
                    FirebaseDatabase
                            .getInstance()
                            .getReference()
                            .child(appointmentDTO.getId())
                            .child("players")
                            .child(name)
                            .setValue(name)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    sendMessage(ListOfPlayersActivity.this, appointmentDTO.getId(),
                                            FirebaseAuth.getInstance().getCurrentUser().getDisplayName() + " se prijavio na termin " + simpleDateFormat.format(new Date(appointmentDTO.getStartTime())),
                                            new Runnable() {
                                                @Override
                                                public void run() {
                                                    subscribe(appointmentDTO.getId(), null);
                                                }
                                            }, appointmentDTO);

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(ListOfPlayersActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }


            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        String id;
        getListOfPlayers(id = getIntent().getStringExtra(APPOINTMENT_ID));
        Log.e("Stefana", " id from list of players -->" + id);
    }

    public void getListOfPlayers(final String appointmentId) {
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child(appointmentId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                appointmentDTO = dataSnapshot.getValue(AppointmentDTO.class);
                if (appointmentDTO != null) {
                    appointmentDTO.setId(dataSnapshot.getKey());

                    getSupportActionBar().setTitle(simpleDateFormat.format(new Date(appointmentDTO.getStartTime())));

                    adapter.players = new ArrayList<>(appointmentDTO.getPlayers().keySet());
                    adapter.notifyDataSetChanged();

                    if (appointmentDTO.getPlayers().containsKey(FirebaseAuth.getInstance().getCurrentUser().getDisplayName())) {
                        Log.e("Stefana", "display name: " + FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                        //korisnik je prijavljen na termina
                        play.setText("Odjavi me");
                    } else {
                        //korisnik nije prijavljen na termin
                        play.setText("Prijavi me");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static void subscribe(String topic, final Runnable action) {


        FirebaseMessaging.getInstance().subscribeToTopic(topic)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (action != null) {
                            new Handler().post(action);
                        }
                    }
                });

    }

    public static void unsubscribe(String topic, final Runnable action) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (action != null) {
                            new Handler().post(action);
                        }

                    }
                });
    }

    public static void sendMessage(final Context context, String topic, String messageValue, final Runnable action, AppointmentDTO appointmentDTO) {
        String url = "https://fcm.googleapis.com/fcm/send";

        final SendMessageRequestDTO requestDTO = new SendMessageRequestDTO();
        requestDTO.setTo("/topics/" + topic);
        DataDTO data = new DataDTO();
        data.setTitle("Basket");
        data.setMessage(messageValue);
        data.setAppointmentId(appointmentDTO.getId());
        requestDTO.setData(data);
        //        MessageDTO msgDto = new MessageDTO();
//        NotificationDTO ntfDTO = new NotificationDTO();
//        ntfDTO.setTitle("Basket");
//        ntfDTO.setBody(messageValue);
//        msgDto.setTopic(topic);
//        msgDto.setNotificationDTO(ntfDTO);
//        requestDTO.setMessage(msgDto);

        StringRequest myReq = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (action != null) {
                            new Handler().post(action);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public byte[] getBody() {
                try {
                    return new ObjectMapper().writeValueAsBytes(requestDTO);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                return "".getBytes();
            }

            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "key=" + YOUR_LEGACY_SERVER_KEY_FROM_FIREBASE_CONSOLE);
                return headers;
            }

        };

        Volley.newRequestQueue(context).add(myReq);
    }


}
